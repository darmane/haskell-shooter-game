module Dibuja(
    pintaEstado
) where

import CodeWorld
import Tipos
import Figuras
import Constantes
import Data.Text (pack)

-- Jugador v Bola

comprobarColisionBolasJugador :: Jugador -> [Bola] -> Bool
comprobarColisionBolasJugador (jx, jy) bolas = any colisionProducida bolas
                                                where colisionProducida ((x,y), r, vx, vy) 
                                                        | jX1 > bX2 || bX1 > jX2 = False
                                                        | jY1 > bY2 || bY1 > jY2 = False
                                                        | otherwise = True
                                                          where jX1 = jx - (personajeHitboxAncho / 2)
                                                                jX2 = jx + (personajeHitboxAncho / 2)
                                                                jY1 = jy - (personajeHitboxLargo / 2)
                                                                jY2 = jy + (personajeHitboxLargo / 2)
                                                                bX1 = x - r
                                                                bX2 = x + r
                                                                bY1 = y - r
                                                                bY2 = y + r

tablero::Picture
tablero = dibujaTablero tableroGrosor (tableroAnchura,tableroAltura)

pintaBolas :: [Bola] -> Picture
pintaBolas bolas = foldl (&) (rectangle 0 0) bolasDibujo
                    where bolasDibujo = map (\((x, y), r, vx, vy) -> dibujaBola r (x, y)) bolas

pintaDisparos::[Disparo] -> Picture
pintaDisparos disparos = foldl (&) (rectangle 0 0) disparosDibujo
                    where disparosDibujo = map (\((x,y), largo, vy) -> dibujaDisparo largo (x, y)) disparos 

pintaFinDePartida :: Int -> Picture
pintaFinDePartida ganador 
                            | ganador == 0  = rectangle 0 0
                            | ganador == 1  = scaled 4 4 (lettering (pack "Gana el jugador 1"))
                            | ganador == 2  = scaled 4 4 (lettering (pack "Gana el jugador 2"))

pintaAccionesIA :: [Accion] -> Picture
pintaAccionesIA acciones = lettering (pack (show acciones))

pintaEstado :: Estado -> Picture
pintaEstado (posB, pT, bolas, ganador, disparos, accionesIA) = dibujaPersonajeJugador posB
                                            & dibujaPersonajeEnemigo pT
                                            & tablero
                                            & coordinatePlane
                                            & pintaBolas bolas
                                            & pintaFinDePartida ganador
                                            & pintaDisparos disparos







