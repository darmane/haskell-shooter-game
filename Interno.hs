{-# LANGUAGE OverloadedStrings #-}

module Interno where

import CodeWorld
import Tipos
import Constantes

-- Jugador

limitesTableroJugador :: Pos -> Pos
limitesTableroJugador (x,y)
    | x > (tableroAnchura / 2) - (personajeHitboxAncho / 2) = (x - 1, y)
    | x < - ( (tableroAnchura / 2) - (personajeHitboxAncho / 2)) = (x + 1, y)
    | otherwise = (x, y)

-- Disparo 

limitesTableroDisparo::[Disparo] -> [Disparo]
limitesTableroDisparo disparos = [d | d<-disparos, comprueba d]
                    where comprueba ((x,y), largo, vy) | c1 y vy largo || c2 y vy largo = False
                                                       | otherwise = True
                          c1 y vy largo = y + vy + largo > (tableroAltura/2)
                          c2 y vy largo = y + vy + largo < -(tableroAltura/2)

actualizaPosDisparo::[Disparo] -> [Disparo]
actualizaPosDisparo disparos = map actualizaPosDisparo disparos
                        where actualizaPosDisparo ((x,y), largo, vy) = ((x,y+vy), largo, vy)

actualizaDisparos::[Disparo]->[Bola]->[Disparo]
actualizaDisparos disparos bolas = actualizaPosDisparo $ (limiteDisparoBola (limitesTableroDisparo disparos) bolas)

    -- colision bola-disparo
compruebaColisionDisparosBola::[Disparo]->Bola-> Bool
compruebaColisionDisparosBola disparos ((x,y), r, vx, vy) = any colisionProducida disparos
                        where colisionProducida ((dx,dy), largo, vyd)
                               | dX1 > bX2 || bX1 > dX2 = False
                               | dY1 > bY2 || bY1 > dY2 = False
                               | otherwise = True
                                 where dX1 = dx - (disparoLargoBase / 4)
                                       dX2 = dx + (disparoLargoBase / 4)
                                       dY1 = dy - (disparoLargoBase / 2)
                                       dY2 = dy + (disparoLargoBase / 2)
                                       bX1 = x - r
                                       bX2 = x + r
                                       bY1 = y - r
                                       bY2 = y + r                   

colisionDisparosBolas::[Disparo]->[Bola]->[Bola]
colisionDisparosBolas disparos bolas = [((x,y), r, vx, -vy) | ((x,y), r, vx, vy) <-bolas, cond ((x,y), r, vx, vy)] ++ bolasAux
            where cond ((x,y), r, vx, vy) = compruebaColisionDisparosBola disparos ((x,y), r, vx, vy)
                  bolasAux = [f ((x,y), r, vx, vy) | ((x,y), r, vx, vy) <-bolas]
                  f ((x,y), r, vx, vy) | compruebaColisionDisparosBola disparos ((x,y), r, vx, vy) = ((x,y), r, -vx, -vy)
                                       | otherwise = ((x,y), r, vx, vy)

limiteDisparoBola::[Disparo] -> [Bola] -> [Disparo]
limiteDisparoBola disparos bolas = [d | d<-disparos, not $ compruebaColisionBolasDisparo bolas d]

limiteDisparosJ1::[Disparo]->Bool
limiteDisparosJ1 disparos = null [((x,y), largo, vy) | ((x,y), largo, vy) <- disparos, vy>0]

limiteDisparosJ2::[Disparo]->Bool
limiteDisparosJ2 disparos = null [((x,y), largo, vy) | ((x,y), largo, vy) <- disparos, vy<0]


compruebaColisionBolasDisparo::[Bola] -> Disparo -> Bool
compruebaColisionBolasDisparo bolas ((dx,dy), largo, dvy) = any colisionProducida bolas
        where colisionProducida ((x,y), r, vx, vy)
                | dX1 > bX2 || bX1 > dX2 = False
                | dY1 > bY2 || bY1 > dY2 = False
                | dvy*vy>=0 = False
                | otherwise = True
                where dX1 = dx - (disparoLargoBase / 4)
                      dX2 = dx + (disparoLargoBase / 4)
                      dY1 = dy - (disparoLargoBase / 2)
                      dY2 = dy + (disparoLargoBase / 2)
                      bX1 = x - r
                      bX2 = x + r
                      bY1 = y - r
                      bY2 = y + r           

-- Bolas

{- Comportamiento de las bolas:
    1 - Comprueba colisiones con tablero
    2 - Actualiza la posición de las bolas
    3 - Comprueba colisiones con jugador (TODO)
-}

limitesTableroBolasX :: [Bola] -> [Bola]
limitesTableroBolasX bolas = map compruebaX bolas
                            where compruebaX ((x,y), r, vx, vy)
                                    | x + vx + r > (tableroAnchura / 2) || x + vx - r < - (tableroAnchura / 2) = ((x - vx, y), r, -vx, vy)
                                    | otherwise = ((x,y), r, vx, vy)

limitesTableroBolasY :: [Bola] -> [Bola]
limitesTableroBolasY bolas = map compruebaY bolas
                            where compruebaY ((x,y), r, vx, vy)
                                    | y + vy + r > (tableroAltura / 2) || y + vy - r < - (tableroAltura / 2) = ((x, y - vy), r, vx, -vy)
                                    | otherwise = ((x,y), r, vx, vy)

actualizaPosBolas :: [Bola] -> [Bola]
actualizaPosBolas bolas = map actualizaPosBola bolas
                            where actualizaPosBola ((x, y), r, vx, vy) = ((x+vx, y+vy), r, vx, vy)

actualizaBolas :: [Disparo] -> [Bola] -> [Bola]
actualizaBolas disparos bolas = colisionDisparosBolas disparos (actualizaPosBolas (limitesTableroBolasY (limitesTableroBolasX bolas)))

-- Colisión Jugador-Bola. Una vez realizada se termina la partida.

comprobarColisionBolasJugador :: Jugador -> [Bola] -> Bool
comprobarColisionBolasJugador (jx, jy) bolas = any colisionProducida bolas
                                                where colisionProducida ((x,y), r, vx, vy) 
                                                        | jX1 > bX2 || bX1 > jX2 = False
                                                        | jY1 > bY2 || bY1 > jY2 = False
                                                        | otherwise = True
                                                          where jX1 = jx - (personajeHitboxAncho / 2)
                                                                jX2 = jx + (personajeHitboxAncho / 2)
                                                                jY1 = jy - (personajeHitboxLargo / 2)
                                                                jY2 = jy + (personajeHitboxLargo / 2)
                                                                bX1 = x - r
                                                                bX2 = x + r
                                                                bY1 = y - r
                                                                bY2 = y + r

{- Manejador de evento principal en función del input
    Flecha izquierda : Mueve el jugador (el que está abajo) a la izquierda
    Flecha derecha : Mueve el jugador a la derecha
    Cualquier input : actualiza bolas
-}

manejaEvento :: Event -> Estado -> Estado
-- Jugador
manejaEvento (KeyPress "A") (pB, pT, bolas, ganador, disparos, acciones) = (newPB, pT, bolas, ganador, disparos, acciones)
    where newPB 
                    | ganador == 0  = limitesTableroJugador ( (fst pB) - 1, snd pB )
                    | otherwise     = pB

manejaEvento (KeyPress "S") (pB, pT, bolas, ganador, disparos, acciones) = (newPB, pT, bolas, ganador, disparos, acciones)
    where newPB 
                    | ganador == 0  = limitesTableroJugador ( (fst pB) + 1, snd pB )
                    | otherwise     = pB

manejaEvento (KeyPress "D") (pB, pT, bolas, ganador, disparos, acciones) = (pB, pT, bolas, ganador, newDisparos, acciones)
    where newDisparos = if limiteDisparosJ1 disparos then newDisparo:disparos else disparos
                   where (x,y) = pB
                         newDisparo = ((x,y), disparoLargoBase, disparoVelocidadBase)

-- IA
manejaEvento (KeyPress "J") (pB, pT, bolas, ganador, disparos, acciones) = (pB, newPT, bolas, ganador, disparos, acciones)
    where newPT 
                    | ganador == 0  = limitesTableroJugador ( (fst pT) - 1, snd pT )
                    | otherwise     = pT

manejaEvento (KeyPress "K") (pB, pT, bolas, ganador, disparos, acciones) = (pB, newPT, bolas, ganador, disparos, acciones)
    where newPT 
                    | ganador == 0  = limitesTableroJugador ( (fst pT) + 1, snd pT )
                    | otherwise     = pT

manejaEvento (KeyPress "L") (pB, pT, bolas, ganador, disparos, acciones) = (pB, pT, bolas, ganador, newDisparos, acciones)
    where newDisparos = if limiteDisparosJ2 disparos then newDisparo:disparos else disparos
                   where (x,y) = pT
                         newDisparo = ((x,y), disparoLargoBase, (-disparoVelocidadBase))



manejaEvento (_) (pB,(pTx, pTy),bolas, ganador, disparos, accionesIA) = (pB, newPT, newBolas, newGanador, newDisparos, newAccionesIA)
    where newBolas 
                    | ganador == 0 = actualizaBolas disparos bolas
                    | otherwise = bolas
          newGanador 
                    | comprobarColisionBolasJugador (pTx, pTy) bolas = 1
                    | comprobarColisionBolasJugador pB bolas = 2
                    | otherwise = 0
          newDisparos
                    | accion == Disparo = ((pTx, pTy), disparoLargoBase, - disparoVelocidadBase) : (actualizaDisparos disparos bolas)
                    | ganador == 0      = actualizaDisparos disparos bolas
                    | otherwise         = disparos
                    where accion = head accionesIA
          newPT     
                    | accion == Izquierda   = (pTx - 1, pTy)
                    | accion == Derecha     = (pTx + 1, pTy)
                    | otherwise = (pTx, pTy)
                    where accion = head accionesIA
          newAccionesIA
                    | length accionesIA == 1    = [Quieto]
                    | otherwise                 = tail accionesIA