module Inicio where

import Tipos
import Constantes

inicio :: Estado
inicio = ( ((0, - tableroAltura / 2 + personajeHitboxLargo / 2 + tableroGrosor)), 
    ((0, tableroAltura / 2 - personajeHitboxLargo / 2 - tableroGrosor)), 
    [generaBola (-2, -1) False],0, [], [Quieto])

    --[generaBola (-4, -1) False, generaBola (-2, -1) False, generaBola (2, -1) False, generaBola (4, -1) False,
    -- generaBola (-4, 1) True, generaBola (-2, 1) True, generaBola (2, 1) True, generaBola (4, 1) True]

generaBola :: Pos -> Bool -> Bola
generaBola (x, y) arriba
                        | arriba =  ((x, y), bolaRadioBase, bolaVelocidadBase, bolaVelocidadBase)
                        | otherwise = ((x, y), bolaRadioBase, - bolaVelocidadBase, - bolaVelocidadBase)
