import Ia
import Inicio
import Tipos
import Constantes

instance Show ArbolBusqueda where
    show (H e) = show e
    show (N e []) = show e
    show (N e (ab1:resto)) = (show ab1) ++ (show resto)

instance Show EstadoBusqueda where
    show (EstadoBusqueda puntuacion accionesTomadas estadoJuego profundidadBusqueda) = (show puntuacion) ++ " " ++ (show accionesTomadas) ++ (show estadoJuego) ++ (show profundidadBusqueda)

-- Tests en consola

inicio1 :: Estado
inicio1 = ( ((0, - tableroAltura / 2 + personajeHitboxLargo / 2 + tableroGrosor)), 
    ((0, tableroAltura / 2 - personajeHitboxLargo / 2 - tableroGrosor)), 
    [generaBola (-2, -1) True],0, [], [Quieto])

e1 = EstadoBusqueda 20 [Izquierda, Izquierda, Derecha] inicio1 3
e2 = EstadoBusqueda 10 [Izquierda, Derecha, Derecha, Derecha, Izquierda] inicio1 5
e3 = EstadoBusqueda 50 [Quieto, Disparo] inicio1 2

arbol = N e3 [H e1, H e2]