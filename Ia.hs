{-# LANGUAGE OverloadedStrings #-}
module Ia where 

import Tipos
import Interno
import CodeWorld
import Constantes
import Data.List

siguienteEstado :: EstadoBusqueda -> Accion -> EstadoBusqueda
siguienteEstado (EstadoBusqueda puntuacion accionesTomadas estadoJuego profundidadBusqueda) accion = EstadoBusqueda nuevaPuntuacion (accionesTomadas ++ [accion]) nuevoEstado nuevaProfundidadBusqueda
                                                                                                                                    where   nuevaPuntuacion = calculaPuntuacion nuevoEstado
                                                                                                                                            nuevoEstado | accion == Izquierda   = manejaEvento (KeyPress "J") (manejaEvento (KeyPress "M") estadoJuego)
                                                                                                                                                        | accion == Derecha     = manejaEvento (KeyPress "K") (manejaEvento (KeyPress "M") estadoJuego)
                                                                                                                                                        | accion == Disparo     = manejaEvento (KeyPress "L") (manejaEvento (KeyPress "M") estadoJuego)
                                                                                                                                                        | accion == Quieto      = manejaEvento (KeyPress "M") estadoJuego
                                                                                                                                            nuevaProfundidadBusqueda = profundidadBusqueda + 1

calculaPuntuacion :: Estado -> Int
calculaPuntuacion ((j1x, j1y), (j2x, j2y), bolas, ganador, disparos, _) = ratioGanador + floor ((distanciaBolasMasCercanaJ2) * 5) + bolasAcercandose * 20 - distanciaDelCentro * 20
                                                                    where   ratioGanador 
                                                                                | ganador == 1  = -100000
                                                                                | ganador == 2  = 100000
                                                                                | otherwise     = 0
                                                                            distanciaBolasMasCercanaJ2 = sum (take 5 (sort (Prelude.map cercaniaJ2 bolas)))
                                                                                            where cercaniaJ2 ((bx, by), _, _, vy)
                                                                                                                                | vy < 0    = 0     -- Las bolas que se alejan de J2 no importan
                                                                                                                                | otherwise = abs (bx - j2x) + abs (by - j2y)
                                                                            bolasAcercandose = sum (map acercandose bolas)
                                                                                            where acercandose ((bx, by), _, _, vy) 
                                                                                                                                | vy < 0 = 1
                                                                                                                                | vy > 0 = -1
                                                                            distanciaDelCentro = floor (abs j2x)

accionesPosibles :: Estado -> [Accion]
accionesPosibles (_, (j2x, j2y), _, ganador, disparos, _) = izq ++ der ++ disp ++ quieto
                                                where   izq 
                                                            | j2x < (tableroAnchura / 2) - (personajeHitboxAncho / 2) = [Derecha]
                                                            | otherwise = []
                                                        der 
                                                            | j2x > - ( (tableroAnchura / 2) - (personajeHitboxAncho / 2)) = [Izquierda]
                                                            | otherwise = []
                                                        disp
                                                            | Prelude.any (\(_, _, dv) -> dv < 0) disparos = []
                                                            | otherwise = [Disparo]
                                                        quieto = [Quieto]



-- Se especifica la profundidad deseada en la búsqueda
busquedaEstados :: Estado -> Int -> ArbolBusqueda
busquedaEstados e profundidadMaxima = busquedaEstadosR (EstadoBusqueda (calculaPuntuacion e) [] e 0) profundidadMaxima


busquedaEstadosR :: EstadoBusqueda -> Int -> ArbolBusqueda
busquedaEstadosR eb profundidadMaxima 
                                            | profundidadBusqueda eb == (profundidadMaxima - 1) = H eb
                                            | otherwise = N eb hijos
                                                where hijos = [busquedaEstadosR (siguienteEstado eb accion) profundidadMaxima | accion <- accionesPosibles (estadoJuego eb)]

obtieneHojas :: ArbolBusqueda -> [EstadoBusqueda]
obtieneHojas (H e) = [e]
obtieneHojas (N e []) = []
obtieneHojas (N e (ab1:resto)) = obtieneHojas ab1 ++ obtieneHojas (N e resto)

obtieneMejoreEstado :: Estado -> EstadoBusqueda
obtieneMejoreEstado e = Prelude.maximum (obtieneHojas (busquedaEstados e parametroProfundiadBusqueda))

obtieneMejoresAcciones :: Estado -> [Accion]
obtieneMejoresAcciones e = accionesTomadas (obtieneMejoreEstado e)