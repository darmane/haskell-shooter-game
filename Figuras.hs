module Figuras where

import CodeWorld
import Tipos
import Constantes

dibujaBola::Double->Pos->Picture
dibujaBola r (x,y) = translated x y (colored purple (solidCircle r))

dibujaDisparo::Double->Pos->Picture
dibujaDisparo largo (x,y) = translated x y (solidRectangle ancho largo)
            where ancho = largo / 2

dibujaTablero::Double->Pos->Picture
dibujaTablero ancho (x,y) = thickRectangle ancho x y

dibujaPersonajeEnemigo::Pos->Picture
dibujaPersonajeEnemigo (x,y) = translated x y (modeloPersonaje pi orange blue black)

dibujaPersonajeJugador::Pos->Picture
dibujaPersonajeJugador (x,y) = translated x y (modeloPersonaje 0 black red black)

modeloPersonaje::Double->Color->Color->Color->Picture
modeloPersonaje rotacion c1 c2 c3 = rotated rotacion (cabeza <> cuerpo <> arma)
                  where cuerpo = colored c2 (solidRectangle personajeHitboxAncho personajeHitboxLargo)
                        cabeza = colored c1 (solidCircle 0.25)
                        arma = translated 0 0.5 (colored c3 (solidRectangle 0.4 1))

