module Constantes where


-- Tablero

tableroGrosor :: Double
tableroGrosor = 0.2
tableroAnchura :: Double
tableroAnchura = 18
tableroAltura :: Double
tableroAltura = 20

-- Personaje

personajeHitboxAncho :: Double
personajeHitboxAncho = 1
personajeHitboxLargo :: Double
personajeHitboxLargo = 1

-- Bola

bolaRadioBase :: Double
bolaRadioBase = 1
bolaVelocidadBase :: Double
bolaVelocidadBase = 0.2

-- Disparo

disparoLargoBase::Double
disparoLargoBase = 0.5
disparoVelocidadBase::Double
disparoVelocidadBase = 2

-- IA

parametroProfundiadBusqueda :: Int
parametroProfundiadBusqueda = 5