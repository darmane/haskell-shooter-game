module Tipos where

type Estado = (Jugador, Jugador, [Bola], Int, [Disparo], [Accion])
{-
    Jugador : El jugador de abajo
    Jugador : El jugador de arriba
    [Bola] : Lista de bolas
    Int : Jugador que ha ganado la partida. 0 si la partida sigue en curso
    [Accion] : Acciones que la IA va a realizar
-}


type Bola = (Pos, Double, Double, Double)
{-  
    Pos : Posicion de la bola
    Double : Radio bola
    Double : Velocidad en el eje x
    Double : Velocidad en el eje y
-}


type Disparo = (Pos, Double, Double)
{-
    Pos : Posicion de la bola
    Double : Largo dibujo disparo
    Double : Velocidad en el eje y
-}


type Pos = (Double, Double)
type Jugador = (Pos)

data Accion = Izquierda
            | Derecha
            | Disparo
            | Quieto
            deriving (Eq)

instance Show Accion where
    show Izquierda = show "Izquierda"
    show Derecha = show "Derecha"
    show Disparo = show "Disparo"
    show Quieto = show "Quieto"

data EstadoBusqueda = EstadoBusqueda    { puntuacion :: Int
                                        , accionesTomadas :: [Accion]
                                        , estadoJuego :: Estado
                                        , profundidadBusqueda :: Int
                                        }

data ArbolBusqueda = H EstadoBusqueda
                    | N EstadoBusqueda [ArbolBusqueda]

instance Eq EstadoBusqueda where
    (EstadoBusqueda puntuacionA _ _ _) == (EstadoBusqueda puntuacionB _ _ _) = puntuacionA == puntuacionB

-- Se ordena de mayor a menor, para que la mejor puntuación esté al principio
instance Ord EstadoBusqueda where
    compare (EstadoBusqueda puntuacionA _ _ _) (EstadoBusqueda puntuacionB _ _ _) = compare puntuacionA puntuacionB