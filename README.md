# LICENCIA

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
    
# DESCRIPCIÓN

Este es un proyecto de clase para la asignatura Programación Declarativa 
del tercer curso de Ingeniería Informática - Tecnologías Informáticas,
realizado por Adrián López Carrillo y José Eulalio Bernáldez Torres.

El proyecto consiste en un videojuego inspirado en Pang, escrito con
el lenguaje de programación Haskell.

El proyecto se compone de un módulo principal donde se programará la
ejecución del juego y un conjunto de módulos secundarios que van a contener
las funciones para dibujar los gráficos, leer los inputs y la toma de decisiones
por IA.

# BUILDS

Hay 3 builds (.exe) en las que se ha usado `ghc -O2 Main.hs`.

**Build - 2 Jugadores** 

No hay IA. Los controles son los siguientes:

A: J1 Izquierda
S: J1 Derecha
D: J1 Disparo
J: J2 Izquierda
K: J2 Derecha
L: J2 Disparo

**Build - IA Normal** 

La IA (jugador de arriba) usa una profundidad de 5 en el árbol de búsqueda. Puede llegar a realizar 1024 simulaciones como máximo. Funciona bien con GHCi, es la versión que está en el código fuente.

**Build - IA Difícil**

La IA (jugador de arriba) usa una profundidad de 7 en el árbol de búsqueda. Puede llegar a realizar 16384 simulaciones como máximo. Es necesario compilarlo con la opción `-O2` para que funcione correctamente.

# IA

Para la IA se ha hecho lo siguiente:

1. Se genera un árbol de búsqueda de una profundidad determinada en el que cada nodo es un `EstadoBusqueda`, estructura de dato que contiene: `puntuacion` del estado (para saber cómo de bueno o de malo es), `accionesTomadas` hasta llegar a este estado, una instancia de `Estado` que contiene toda la información del juego en un instante determinado y la `profundidad` de ese nodo en el árbol de búsqueda.
    - En cada nodo se puede tomar una `Accion` por cada `accionesPosibles` que puede realizar el Jugador 2 en función del `Estado` del nodo. Son 4 Acciones posibles: `Izquierda`, `Derecha`, `Quieto`, `Disparo`.
    - El `EstadoBusqueda` siguiente se calcula con la función `siguienteEstado`, que toma un `EstadoBusqueda` y una `Accion`, coge el `Estado` del `EstadoBusqueda` y usa la función `manejaEvento` que se usa en el motor del juego para obtener el nuevo `Estado`
    - La `puntuacion`del nuevo `Estado` se calcula con la función `calculaPuntuacion`. Se da prioridad a permanecer en el centro, a permanecer lejos de las 5 bolas más cercanas, a que haya más bolas que se alejan de las que se acercan y a sobrevivir.

2. Se obtienen todas las hojas del árbol
3. Se obtiene la hoja (`EstadoBusqueda`) con mayor puntuación y se obtienen la lista `accionesTomadas` hasta llegar a ese `EstadoBusqueda`
4. Las `accionesTomadas` se pasan al `Estado` del juego, que las va realizando por el J2 hasta que la lista se vacía y se repite el proceso.

